from textblob import TextBlob

# Analise de sentimento utilizando Naive Bayes.
# A biblioteca TextBlob utiliza o metodo de Bayes
# "treinando" o algoritmo em ingles baseado nos corpus
# da biblioteca nltk (natural language toolkit).
def analisar_sentimento(palavras):
    palavras = " ".join(palavras)
    polaridade = TextBlob(palavras)

    return polaridade.sentiment

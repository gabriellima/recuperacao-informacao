# -*- coding: utf-8 -*-

from nltk.corpus import stopwords
from nltk import FreqDist

import string

# remover todas as palavras nao relevantes (stopwords) para a pesquisa
def remover_stop_words(sentence):
    stop_words = stopwords.words('english')
    words = []
    for word in sentence.split():
        word = word.lower()
        if word not in stop_words:
            if verifica_link_palavra(word):
                words.append(word)
            else:
                word = remover_pontuacoes(word)
                if not word == '':
                    words.append(word)
    return words

# frequencia da palavra em um texto
def distribuicao_frequencia(sentence):
    frequencia_palavras = FreqDist(sentence)
    return frequencia_palavras.most_common()

# se palabra for link, nao remover pontuacao
def verifica_link_palavra(palavra):
    return palavra.startswith('http') or palavra.startswith('https')

# se caracter for pontuacao, verifica se esta na lista
# se estiver, remova-o
def remover_pontuacoes(palavra):
    # lista com todas as pontuacoes a serem removidas do texto
    pontuacoes_para_remocao = set(string.punctuation)
    # importante nao remover hashtag
    # para o trabalho mostrar na consulta a hashtag com o caracter #
    pontuacoes_para_remocao.remove('#')
    return ''.join(ch for ch in palavra if ch not in pontuacoes_para_remocao)

# junta todos os textos dos tweets em um unico texto
def join_palavras(conjunto_palavras):
    todas_palavras = ""
    for palavras in conjunto_palavras:
        todas_palavras += palavras
    return todas_palavras

# calcula o percentual da polaridade de um texto
def calcular_porcentagem(sentimento):
    polaridade = sentimento * 100
    return "{0:.2f}".format(round(polaridade))

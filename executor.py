from tweeter_api import busca_tweets
from text_helper import remover_stop_words, distribuicao_frequencia
from gplus_api import *
from analise_sentimento import analisar_sentimento

tweets = busca_tweets('#amazing', 50)

for tweet in tweets:
    palavras = remover_stop_words(tweet.text)

    print palavras
    print distribuicao_frequencia(palavras)
    print analisar_sentimento(palavras)

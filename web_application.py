from flask import Flask
from flask import render_template, request

from tweeter_api import busca_tweets
from text_helper import remover_stop_words, distribuicao_frequencia, join_palavras, \
                        calcular_porcentagem

from analise_sentimento import analisar_sentimento

app = Flask(__name__)

@app.route("/", methods=['GET', 'POST'])
def home():
    if request.method == 'GET':
        return render_template("index.html")
    else:
        processados = []
        # parametros do formulario
        hashtag = request.form["hashtag"]
        profundidade = request.form["profundidade"]
        filtro_polaridade = request.form["polaridade"]
        booleano = request.form["booleano"]
        # quantos tweets ira retornar
        if not profundidade:
            profundidade = 10
        # busca todos os tweets que contem a hashtag
        tweets = busca_tweets(hashtag, profundidade)

        # encontrar a frequencia dos termos em todas as
        # palavras de todos os tweets
        todas_as_palavras = join_palavras(" ".join(tweets))
        todas_as_palavras = remover_stop_words(todas_as_palavras)
        frequencia_termos = distribuicao_frequencia(todas_as_palavras)

        for tweet in tweets:
            palavras = remover_stop_words(tweet)
            sentimento = analisar_sentimento(palavras)
            polaridade = calcular_porcentagem(sentimento.polarity)

            # filtro booleano
            if booleano and booleano not in tweet:
                continue

            # filtro polaridade
            if filtro_polaridade == 'negativa' and sentimento.polarity >= 0:
                continue
            elif filtro_polaridade == 'positiva' and sentimento.polarity <= 0:
                continue

            subjetividade = calcular_porcentagem(sentimento.subjectivity)
            processados.append({'original': tweet,
                                'palavras': palavras,
                                'sentimento': float(polaridade),
                                'subjetividade': subjetividade})

        return render_template("index.html", processados=processados, frequencia_termos=frequencia_termos)

if __name__ == "__main__":
    app.debug = True
    app.run()
